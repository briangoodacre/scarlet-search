<?php
include('includes/before_html.php');
$tables = array('Interests', 'Employment_Experience', 'Enrolled', 'ClubMembers','Relationships');

function generateFields($table, $ruid) {
    $excluded = array('Person1', 'Person2', 'id', 'RUID');
    $excludedTables = array('Enrolled', 'ClubMembers');
    $query = "SELECT * FROM $table WHERE RUID=$ruid";
    $result = mysql_query($query);

    $count = mysql_num_rows($result);

    echo "<table>\n";

    if ($table == 'Relationships') {
        $result = mysql_query("describe $table");
        while ($row = mysql_fetch_array($result)) {
            if (!in_array($row['Field'], $excluded)) {
                echo "<tr>";
                if ($row['Null'] == 'NO')
                    echo "<td><strong><font color='red'>*" . $row['Field'] . "</font></strong></td>";
                else
                    echo "<td><strong>" . $row['Field'] . "</strong></td>";
                echo "<td><input name='{$table}_{$row['Field']}' type='text' id='{$row['Field']}'/></td>";
                echo "</tr>";
            } else if ($row['Field'] == 'Person2') {
                echo "<tr>";
                if ($row['Null'] == 'NO')
                    echo "<td><strong><font color='red'>*" . $row['Field'] . "</font></strong></td>";
                else
                    echo "<td><strong>" . $row['Field'] . "</strong></td>";
                echo "<td>";
                echo "<select name='{$table}_{$row['Field']}'>";
                $sql = "select U.FirstName, U.LastName, U.RUID FROM Users U, Friends F where F.Person1=" . $ruid . " AND U.RUID=F.Person2 AND F.Status='Accepted'";
                $resultA = mysql_query($sql);
                echo '<option value=""></option>';
                while ($rowA = mysql_fetch_array($resultA)) {
                    echo '<option value="' . $rowA['RUID'] . '">' . $rowA['FirstName'] . ' ' . $rowA['LastName'] . ' - ' . $rowA['RUID'] . '</option>';
                }
                echo "</select>";
                echo "</td>";
                echo "</tr>";
            }
        }
    } else {

        $result = mysql_query("describe $table");
        while ($row = mysql_fetch_array($result)) {
            if (!in_array($row['Field'], $excluded) && !in_array($table, $excludedTables)) {
                echo "<tr>";
                if ($row['Null'] == 'NO')
                    echo "<td><strong><font color='red'>*" . $row['Field'] . "</font></strong></td>";
                else
                    echo "<td><strong>" . $row['Field'] . "</strong></td>";
                echo "<td><input name='{$table}_{$row['Field']}' type='text' id='{$row['Field']}'/></td>";
                echo "</tr>";
            } else if (!in_array($row['Field'], $excluded) && $table == 'Enrolled') {
                echo "<tr>";
                if ($row['Null'] == 'NO')
                    echo "<td><strong><font color='red'>*" . $row['Field'] . "</font></strong></td>";
                else
                    echo "<td><strong>" . $row['Field'] . "</strong></td>";
                echo "<td>";
                echo "<select name='{$table}_{$row['Field']}'>";
                $sql = "select * from Courses where id NOT IN (SELECT CourseId FROM Enrolled WHERE RUID=$ruid)";
                $resultA = mysql_query($sql);
                while ($rowA = mysql_fetch_array($resultA)) {
                    echo '<option value="' . $rowA['id'] . '">' . $rowA['id'] . ' - ' . $rowA['Title'] . ' - ' . $rowA['Semester'] . '</option>';
                }
                echo "</select>";
                echo "</td>";
                echo "</tr>";
            } else if (!in_array($row['Field'], $excluded) && $table == 'ClubMembers') {
                echo "<tr>";
                if ($row['Null'] == 'NO')
                    echo "<td><strong><font color='red'>*" . $row['Field'] . "</font></strong></td>";
                else
                    echo "<td><strong>" . $row['Field'] . "</strong></td>";
                echo "<td>";
                echo "<select name='{$table}_{$row['Field']}'>";
                $sql = "select C.Name from Clubs C where C.Name NOT IN (SELECT M.ClubName FROM ClubMembers M WHERE M.RUID=$ruid)";
                $resultA = mysql_query($sql);
                while ($rowA = mysql_fetch_array($resultA)) {
                    echo '<option value="' . $rowA['Name'] . '">' . $rowA['Name'] . '</option>';
                }
                echo "</select>";
                echo "</td>";
                echo "</tr>";
            }
        }
    }
    echo '<tr><td colspan="2"><input style="width: 100px;" type="submit" name="Submit" value="Add"/></td></tr>';
    echo '</table>';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <title><?php echo $heading ?></title>
        <?php
        include('includes/head.php');

        echo '<script type="text/javascript">';
        foreach ($tables as $t) {
            echo " 
            $(document).ready(function() { 
                $('#{$t}_form').ajaxForm({ 
                    target: '#{$t}_target', 
                    success: function() { 
                        $('#{$t}_target').fadeIn('slow'); 
        } 
                }); 
            });";
        }
        echo '</script> ';
        ?>


    </head>

    <body>
        <div id="wrapper">
            <?php include('includes/header.php'); ?>
            <?php include('includes/nav.php'); ?>
            <div id="content">
                <?php
                $db = mysql_connect(db_server, $db_user, $db_password);
                mysql_select_db($db_dbname);

                foreach ($tables as $table) {
                    echo "<h3>{$table}</h3><br>";
                    echo '<form id="' . $table . '_form" name="' . $table . '_form" method="post" action="add_info_return.php?t=' . $table . '">';
                    generateFields($table, $myRuid);
                    echo '</form>';
                    echo "<div id='{$table}_target'></div><br></br>";
                }
                ?>
            </div> <!-- end #content -->
            <?php include('includes/sidebar.php'); ?>
            <?php include('includes/footer.php'); ?>
        </div> <!-- End #wrapper -->
    </body>
</html>
