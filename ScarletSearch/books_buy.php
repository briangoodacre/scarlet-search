<!--bg-->
<?php
$table='Books';
include('includes/before_html.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <?php include('includes/head.php'); ?>
        <title><?php echo $heading ?></title>

    </head>

    <body>
        <div id="wrapper">
            <?php include('includes/header.php'); ?>
            <?php include('includes/nav.php'); ?>
            <div id="content">
                    <?php
                    echo "<h3>Scarlet Searches's Books for Sale</h3><BR>";
                    $db = mysql_connect(db_server, $db_user, $db_password);
                    mysql_select_db($db_dbname);
                    $sql="SELECT B.ISBN, B.Title, S.RUID, S.Price, S.Condition_, S.Description, U.FirstName, U.LastName, U.Email FROM Books B, Selling S, Users U WHERE (B.ISBN=S.ISBN AND U.RUID=S.RUID)";
                    echo "SQL QUERY:&nbsp;".$sql."<BR><BR>";
                    $result2 = mysql_query($sql);
                    $count = mysql_num_rows($result2);
                    
                    if($count==0){
                        echo "There are no books currently for sale.";
                    }
                        
                    while ($row = mysql_fetch_array($result2)) {
                        echo "<table>";
                          {
                              echo "<tr><td><strong> ". $row['FirstName']." ". $row['LastName']. " </strong></td></tr>";
			      echo "<tr><td><strong> ". $row['Email']. " </strong></td></tr>";
                              echo "<tr><td><strong> ". $row['RUID']. " </strong></td></tr>";
                              echo "<tr><td> ". "Title: ".$row['Title']. " </td></tr>";
                              echo "<tr><td> ". "ISBN: ".$row['ISBN']. " </td></tr>";
			      echo "<tr><td> ". "Price: $".$row['Price']. " </td></tr>";
			      echo "<tr><td> ". "Condition: ".$row['Condition_']. " </td></tr>";
			      echo "<tr><td> ". "Description: ".$row['Description']. " </td></tr>";
                          }
                        echo "</table><BR>";
                    }
                    mysql_close($db)
                    ?>               
            </div> <!-- end #content -->
            <?php include('includes/sidebar.php'); ?>
            <?php include('includes/footer.php'); ?>
        </div> <!-- End #wrapper -->
    </body>
</html>