<?php include('includes/before_html.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />

        <meta name="keywords" content="" />

        <meta name="author" content="" />

        <?php include('includes/head.php'); ?>

        <title><?php echo $heading ?></title>

    </head>

    <body>

        <div id="wrapper">

            <?php include('includes/header.php'); ?>

            <?php include('includes/nav.php'); ?>

            <div id="content">
                <h3>Compose Message</h3><br></br>

                <?php
                $db = mysql_connect(db_server, $db_user, $db_password);
                mysql_select_db($db_dbname);
                if (isset($_GET['replyto'])) {
                    $replyto = "?replyto=" . $_GET['replyto'];
                    $result = mysql_query("select M.FromID from Messages M where M.id=" . $_GET['replyto']);
                    $row = mysql_fetch_array($result);
                    $replytoRUID = $row['FromID'];
                }
                else
                    $replyto = '';

                echo '<form name="form1" method="post" action="send_message.php' . $replyto . '">';
                echo "<table>";
                echo "<tr><td>To:</td><td>";
                //make dropdown
                echo "<select name='to'>";

                $result = mysql_query("select U.FirstName, U.LastName, U.RUID FROM Users U, Friends F where F.Person1=" . $myRuid . " AND U.RUID=F.Person2 AND F.Status='Accepted'");
                while ($row = mysql_fetch_array($result)) {
                    if ($row['RUID'] == $replytoRUID)
                        echo '<option selected value="' . $row['RUID'] . '">' . $row['FirstName'] . ' ' . $row['LastName'] . ' - ' . $row['RUID'] . '</option>';
                    else
                        echo '<option value="' . $row['RUID'] . '">' . $row['FirstName'] . ' ' . $row['LastName'] . ' - ' . $row['RUID'] . '</option>';
                }
                echo "</select>";
                echo "</td></tr>";

                echo '<tr><td>Subject</td><td><input type="text" name="subject" /></td></tr>';
                echo '<tr><td>Body</td><td><textarea id="body" name="body" rows="5" cols="60"></textarea></td></tr>';
                echo '<tr><td></td><td><input type="submit" name="Submit" value="Send"/></td></tr>';
                echo "</table></form>";
                ?>
            </div> <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>

            <?php include('includes/footer.php'); ?>

        </div> <!-- End #wrapper -->

    </body>

</html>