<?php include('includes/before_html.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />

        <meta name="keywords" content="" />

        <meta name="author" content="" />

        <?php include('includes/head.php'); ?>

        <title><?php echo $heading ?></title>

    </head>

    <body>

        <div id="wrapper">

            <?php include('includes/header.php'); ?>

            <?php include('includes/nav.php'); ?>

            <div id="content">
                <?php
                $db = mysql_connect(db_server, $db_user, $db_password);
                mysql_select_db($db_dbname);
                $sql = "SELECT U.FirstName, U.LastName, U.RUID, C.id, C.Title, C.Semester FROM Users U, Courses C, Enrolled E, Friends F WHERE F.Person1=" . $myRuid . " AND F.Person2=E.RUID  AND F.Status='Accepted' AND E.CourseId=C.id AND C.id=" . $_GET['id'] . " AND U.RUID=E.RUID";
                $result = mysql_query($sql);

                //output data in a table
                $output = '';
                $output.= "<div id='output'>\n";
                $output.= "<table width='100%'>\n";
                $output.= "<tr>";
                $output.= "<th>Name</th><th>Course ID</th><th>Course</th>";
                $output.= "</tr>";
                while ($row = mysql_fetch_array($result)) {
                    $output.= "<tr>\n";
                    $output.= "<td><a href='dashboard.php?ruid=".$row['RUID']."'>" . $row['FirstName'] . " " . $row['LastName'] . "</a></td>";
                    $output.= "<td>" . $row['id'] . "</td>";
                    $output.= "<td>" . $row['Title'] . " - " . $row['Semester'] . "</td>";
                    $output.= "</tr>\n";
                }
                $output.= '</table></div>';
                
                echo $output;
                ?>               
            </div> <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>

            <?php include('includes/footer.php'); ?>

        </div> <!-- End #wrapper -->

    </body>

</html>