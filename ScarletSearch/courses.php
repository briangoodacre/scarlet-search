<?php
$table = 'Courses';
include('includes/before_html.php');
if ($friend_page)
    checkSecurity($table);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <title><?php echo $heading ?></title>
        <?php include('includes/head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('includes/header.php'); ?>
            <?php include('includes/nav.php'); ?>
            <div id="content">

                <?php
                echo "<h3>" . $getName . "'s $table</h3><BR>";
                $db = mysql_connect(db_server, $db_user, $db_password);
                mysql_select_db($db_dbname);
                $sql = "SELECT C.id, C.Title, C.Semester FROM Enrolled E, Courses C WHERE E.RUID = " . $getRuid . " AND E.CourseId=C.id";
                echo "SQL QUERY:&nbsp;" . $sql . "<BR><BR>";
                $result2 = mysql_query($sql);
                $count = mysql_num_rows($result2);

                if ($count == 0) {
                    echo "No courses.";
                }

                outputQueryResults($sql);

                if ($myRuid == $getRuid) {
                    // Add show all courses that your friends are taking and how many of your friends are taking them
                    echo "<br></br><h3>See what courses your friends are in!</h3>";
                    $sql = "SELECT DISTINCT C.id, C.Title, C.Semester, count(*) as '# Friends Enrolled' FROM Courses C, Enrolled E, Friends F WHERE F.Person1=$myRuid AND F.Person2=E.RUID AND F.Status='Accepted' AND E.CourseId=C.id GROUP BY C.id ORDER BY count(*) DESC;";
                    $result = mysql_query($sql);

                    //output data in a table
                    $output = '';
                    $output.= "<div id='output'>\n";
                    $output.= "<table width='100%'>\n";
                    $output.= "<tr>";
                    for ($i = 0; $i < mysql_num_fields($result); $i++) {
                        $field_info = mysql_fetch_field($result, $i);
                        $output.= "<th>{$field_info->name}</th>";
                    }
                    $output.= "</tr>";
                    while ($row = mysql_fetch_array($result)) {
                        $output.= "<tr>\n";
                        for($i=0;$i<count($row)/2;$i++){
                        if($row[$i]==$row['# Friends Enrolled'])
                            $output.= "<td><a href='course_friends.php?id=".$row['id']."'>".$row[$i]."</a></td>\n";
                        else
                            $output.= "<td>$row[$i]</td>\n";
                        }
                        $output.= "</tr>\n";
                    }
                    $output.= '</table></div>';

                    $count = mysql_num_rows($result);
                    if ($count <= 0)
                        echo "Your friends are not enrolled in any courses.";
                    else {
                        echo $output;
                    }
                }
                mysql_close($db)
                ?>
            </div>
            <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>
            <?php include('includes/footer.php'); ?>
        </div>
        <!-- End #wrapper -->

    </body>
</html>