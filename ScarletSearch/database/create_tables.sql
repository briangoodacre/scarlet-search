DROP Table Users;
DROP Table Interests;
DROP Table Employment_Experience;
DROP Table Relationships;
DROP Table Courses;
DROP Table Enrolled;
DROP Table Books;
DROP Table Selling;
DROP Table CourseTextBooks;
DROP Table Clubs;
DROP Table ClubMembers;
DROP Table Messages;
DROP Table Housing;
DROP Table Parties;
DROP Table StudyGroups;
DROP Table StudyGroupMembers;
DROP Table Friends;
DROP Table Status;
DROP Table Comments;
DROP Table PartiesMembers;
DROP Table Security;
DROP Table SavedQueries;


CREATE TABLE Users (
    FirstName CHAR(20) NOT NULL,
    LastName CHAR(20) NOT NULL,
    Birthdate DATE,
    Email CHAR(50) NOT NULL,
    RUID INTEGER NOT NULL,
    PhoneNumber INTEGER,
    Address CHAR(100),
    Gender CHAR(10),
    NetID CHAR(20) NOT NULL,
    EnrollYear INTEGER,
    GradYear INTEGER,
    Password CHAR(20) NOT NULL,
    PRIMARY KEY(RUID));

CREATE TABLE Interests (
    RUID INTEGER NOT NULL,
    InterestName CHAR(50) NOT NULL,
    PRIMARY KEY(RUID,InterestName),
    FOREIGN KEY (RUID) REFERENCES Users ON DELETE CASCADE);
   
CREATE TABLE Employment_Experience (
    id INTEGER NOT NULL AUTO_INCREMENT,
    JobTitle CHAR(20),
    Employer CHAR(50) NOT NULL,
    Employer_Address CHAR(100),
    Salary DOUBLE,
    RUID INTEGER NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (RUID) REFERENCES Users ON DELETE CASCADE);

CREATE TABLE Relationships (
    Person1 INTEGER,
    Person2 INTEGER NOT NULL,
    Since DATE,
    RelationshipType CHAR(20),
    PRIMARY KEY(Person1,Person2,RelationshipType),
    FOREIGN KEY(Person1) REFERENCES Users(RUID) ON DELETE CASCADE,
    FOREIGN KEY(Person2) REFERENCES Users(RUID) ON DELETE CASCADE);

CREATE TABLE Courses (
    id INTEGER NOT NULL,
    Title CHAR(20) NOT NULL,
    Semester CHAR(20),
    PRIMARY KEY(id));

CREATE TABLE Enrolled (
    RUID INTEGER,
    CourseId INTEGER,
    PRIMARY KEY(RUID,CourseId),
    FOREIGN KEY(RUID) REFERENCES Users ON DELETE CASCADE,
    FOREIGN KEY(CourseId) REFERENCES Courses(id) ON DELETE CASCADE);

CREATE TABLE Books (
    ISBN INTEGER,
    Title CHAR(30) NOT NULL,
    PRIMARY KEY (ISBN));

CREATE TABLE Selling (
    RUID INTEGER,
    ISBN INTEGER,
    Price DOUBLE NOT NULL,
    Condition_ CHAR(20) NOT NULL,
    Description TEXT,
    PRIMARY KEY(RUID,ISBN),
    FOREIGN KEY(RUID) REFERENCES Users(RUID) ON DELETE CASCADE,
    FOREIGN KEY(ISBN) REFERENCES Books(ISBN) ON DELETE CASCADE);

CREATE TABLE CourseTextBooks (
    CourseId INTEGER,
    ISBN INTEGER,
    PRIMARY KEY(CourseId, ISBN),
    FOREIGN KEY(ISBN) REFERENCES Books(ISBN) ON DELETE CASCADE,
    FOREIGN KEY(CourseId) REFERENCES Courses(id) ON DELETE CASCADE);

CREATE TABLE Clubs (
    Name CHAR(30),
    Description TEXT NOT NULL,
    MeetingTimes TEXT NOT NULL,
    President INTEGER,
    PRIMARY KEY (Name),
    FOREIGN KEY (President) REFERENCES Users(RUID) ON DELETE SET NULL);

CREATE TABLE ClubMembers (
    ClubName CHAR(30),
    RUID CHAR(30),
    PRIMARY KEY(ClubName,RUID),
    FOREIGN KEY(RUID) REFERENCES Users ON DELETE CASCADE,
    FOREIGN KEY(ClubName) REFERENCES Clubs(Name) ON DELETE CASCADE);

CREATE TABLE Messages (
    id INTEGER NOT NULL AUTO_INCREMENT,   
    ToID INTEGER NOT NULL,
    FromID INTEGER NOT NULL,
    Subject CHAR(40),
    Date DATETIME,
    ReplyTo INTEGER,
    Body TEXT,
    Owner INTEGER NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (ToID) REFERENCES Users(RUID) ON DELETE NO ACTION,
    FOREIGN KEY (FromID) REFERENCES Users(RUID) ON DELETE NO ACTION,
    FOREIGN KEY (Owner) REFERENCES Users(RUID) ON DELETE NO ACTION,
    FOREIGN KEY (ReplyTo) REFERENCES Messages(id) ON DELETE NO ACTION);

CREATE TABLE Housing (
    BuildingName CHAR(50),
    RoomNumber INTEGER,
    RUID INTEGER,
    PRIMARY KEY (RUID),
    FOREIGN KEY (RUID) REFERENCES Users ON DELETE CASCADE);

CREATE TABLE Parties (
    PartyName CHAR(30),
    HostRUID INTEGER, 
    Time CHAR(30),
    Description TEXT,
    BuildingName CHAR(50),
    RoomNumber INTEGER,
    PRIMARY KEY (PartyName),
    FOREIGN KEY (HostRUID) REFERENCES Users ON DELETE CASCADE,
    FOREIGN KEY (BuildingName) REFERENCES Housing ON DELETE CASCADE,
    FOREIGN KEY (RoomNumber) REFERENCES Housing ON DELETE SET NULL);

CREATE TABLE PartiesMembers (
    PartyName CHAR(30),
    RUID INTEGER,
    PRIMARY KEY (PartyName,RUID),
    FOREIGN KEY (PartyName) REFERENCES Parties ON DELETE CASCADE,
    FOREIGN KEY (RUID) REFERENCES Users ON DELETE CASCADE);

CREATE TABLE StudyGroups (
    Name CHAR(20),
    MeetingTimes TEXT,
    Leader INTEGER,
    Max_Size INTEGER,
    Location CHAR(50),
    CourseId INTEGER,
    PRIMARY KEY(Name),
    FOREIGN KEY (Leader) REFERENCES Users(RUID) ON DELETE SET NULL,
    FOREIGN KEY (CourseId) REFERENCES Courses(id) ON DELETE NO ACTION);

CREATE TABLE StudyGroupMembers (
    GroupName CHAR(20),
    RUID INTEGER,
    PRIMARY KEY (GroupName, RUID),
    FOREIGN KEY (GroupName) REFERENCES StudyGroups (Name) ON DELETE CASCADE,
    FOREIGN KEY (RUID) REFERENCES Users ON DELETE CASCADE);

CREATE TABLE Friends (
    Person1 INTEGER,
    Person2 INTEGER,
    Status CHAR(20) NOT NULL,
    PRIMARY KEY(Person1, Person2),
    FOREIGN KEY (Person1) REFERENCES Users (RUID) ON DELETE CASCADE,
FOREIGN KEY (Person2) REFERENCES Users (RUID) ON DELETE CASCADE);

CREATE TABLE Status (
    id INTEGER NOT NULL AUTO_INCREMENT,
    RUID INTEGER,
    Text TEXT,
    TimePosted DATETIME,
    PRIMARY KEY(id),
    FOREIGN KEY (RUID) REFERENCES Users ON DELETE CASCADE);

CREATE TABLE Comments (
    Person1 INTEGER,
    Person2 INTEGER,
    Comment TEXT,
    TimePosted Date,
    PRIMARY KEY(Person1, Person2),
    FOREIGN KEY (Person1) REFERENCES Users (RUID) ON DELETE CASCADE,
    FOREIGN KEY (Person2) REFERENCES Users (RUID) ON DELETE CASCADE);

CREATE TABLE SavedQueries (
	id INTEGER NOT NULL AUTO_INCREMENT,
	Query TEXT,
	PageTitle VARCHAR(30),
	PRIMARY KEY (id)
);

CREATE TABLE Security (
	RUID INTEGER NOT NULL,
	Profile TINYINT NOT NULL DEFAULT 1,
	Relationship  TINYINT NOT NULL DEFAULT 0,
	Housing TINYINT NOT NULL DEFAULT 0,
	Interests  TINYINT NOT NULL DEFAULT 0,
	Employment TINYINT NOT NULL DEFAULT 0,
	Courses TINYINT NOT NULL DEFAULT 0,
        Parties TINYINT NOT NULL DEFAULT 0,
        Clubs TINYINT NOT NULL DEFAULT 0,
	Study_Groups TINYINT NOT NULL DEFAULT 0,
	PRIMARY KEY(RUID),
	FOREIGN KEY (RUID) REFERENCES Users ON DELETE CASCADE);