<?php

function ListFiles($dir) {

    if ($dh = opendir($dir)) {

        $files = Array();
        $inner_files = Array();

        while ($file = readdir($dh)) {
            if ($file != "." && $file != ".." && $file[0] != '.') {
                if (is_dir($dir . "/" . $file)) {
                    $inner_files = ListFiles($dir . "/" . $file);
                    if (is_array($inner_files))
                        $files = array_merge($files, $inner_files);
                } else {
                    array_push($files, $dir . "/" . $file);
                }
            }
        }

        closedir($dh);
        return $files;
    }
}

echo "<table>";
foreach (ListFiles('.') as $key => $file) {
    echo "<tr><td>".$file . "</td><td></td></tr>";
}
echo "</table>";
?>