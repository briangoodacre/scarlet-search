<?php
include('includes/before_html.php');

function generateFields($table, $ruid) {
    $excluded = array('Person1', 'Person2', 'id', 'RUID');
    $query = "SELECT * FROM $table WHERE RUID=$ruid";
    $result = mysql_query($query);

    $count = mysql_num_rows($result);

    echo "<table>\n";

    if ($count > 0) {
        while ($row = mysql_fetch_row($result)) {

            for ($i = 0; $i < mysql_num_fields($result); $i++) {
                $field_info = mysql_fetch_field($result, $i);
                if (!in_array($field_info->name, $excluded)) {
                    echo "<tr>";
                    echo "<td><strong>{$field_info->name}</strong></td>";
                    echo "<td><input name='{$table}_{$field_info->name}' type='text' id='{$table}_{$field_info->name}' value='" . $row[$i] . "'/></td>";
                    echo "</tr>";
                } else if ($field_info->name == 'Person2') {
                    echo "<tr>";
                    echo "<td><strong>{$field_info->name}</strong></td>";
                    echo "<td>";
                    echo "<select name='{$table}_{$field_info->name}'>";

                    $resultA = mysql_query("select U.FirstName, U.LastName, U.RUID FROM Users U, Friends F where F.Person1=" . $myRuid . " AND U.RUID=F.Person2 AND F.Status='Accepted'");
                    while ($rowA = mysql_fetch_array($resultA)) {
                        echo '<option value="' . $rowA['RUID'] . '">' . $rowA['FirstName'] . ' ' . $rowA['LastName'] . ' - ' . $rowA['RUID'] . '</option>';
                    }
                    echo "</select>";
                    echo "</td>";
                    echo "</tr>";
                }
            }
        }
    } else {
        $result = mysql_query("describe $table");
        while ($row = mysql_fetch_array($result)) {
            if (!in_array($row['Field'], $excluded)) {
                echo "<tr>";
                echo "<td><strong>" . $row['Field'] . "</strong></td>";
                echo "<td><input name='{$table}_{$row['Field']}' type='text' id='{$row['Field']}'/></td>";
                echo "</tr>";
            } else if ($row['Field'] == 'Person2') {
                echo "<tr>";
                echo "<td><strong>{$row['Field']}</strong></td>";
                echo "<td>";
                echo "<select name='{$table}_{$row['Field']}'>";
                $sql = "select U.FirstName, U.LastName, U.RUID FROM Users U, Friends F where F.Person1=" . $ruid . " AND U.RUID=F.Person2 AND F.Status='Accepted'";
                $resultA = mysql_query($sql);
                echo '<option value=""></option>';
                while ($rowA = mysql_fetch_array($resultA)) {
                    echo '<option value="' . $rowA['RUID'] . '">' . $rowA['FirstName'] . ' ' . $rowA['LastName'] . ' - ' . $rowA['RUID'] . '</option>';
                }
                echo "</select>";
                echo "</td>";
                echo "</tr>";
            }
        }
    }
    echo '</table><br></br>';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <?php include('includes/head.php'); ?>
        <title><?php echo $heading ?></title>
        <script type="text/javascript">
            $(document).ready(function() { 
                $('#form1').ajaxForm({ 
                    target: '#responseTarget', 
                    success: function() { 
                        $('#responseTarget').fadeIn('slow'); 
                    } 
                }); 
            });    
        </script>

    </head>

    <body>
        <div id="wrapper">
            <?php include('includes/header.php'); ?>
            <?php include('includes/nav.php'); ?>
            <div id="content">
                <form id="form1" name="form1" method="post" action="save_profile.php">
                    <?php
                    $db = mysql_connect(db_server, $db_user, $db_password);
                    mysql_select_db($db_dbname);
                    $tables = array('Users', 'Housing');
//generateFields("SELECT FirstName, LastName, Birthdate, Email, PhoneNumber, Address, Gender, NetID, EnrollYear, GradYear FROM Users WHERE RUID=$myRuid");
                    foreach ($tables as $table) {
                        echo "<h3>{$table}</h3><br>";
                        generateFields($table, $myRuid);
                    }
                    ?>
                    <input type="submit" name="Submit" value="Save"/>
                </form>
                <div id ="responseTarget"></div>
            </div> <!-- end #content -->
            <?php include('includes/sidebar.php'); ?>
            <?php include('includes/footer.php'); ?>
        </div> <!-- End #wrapper -->
    </body>
</html>