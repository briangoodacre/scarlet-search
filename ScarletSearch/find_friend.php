<?php
include('includes/before_html.php');
$db = mysql_connect(db_server, $db_user, $db_password);
if (!$db) {
    //die('Could Not Connect: ' . mysql_error());
} else {
    //echo "Connected Successfully...\n";
}

mysql_select_db($db_dbname);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />

        <meta name="keywords" content="" />

        <meta name="author" content="" />

        <?php include('includes/head.php'); ?>

        <title><?php echo $heading ?></title>

        <script language="javascript"> 
            function toggle() {
                var ele = document.getElementById("toggleText");
                var text = document.getElementById("displayText");
                if(ele.style.display == "block") {
                    ele.style.display = "none";
                    text.innerHTML = "Show Advanced Options";
                }
                else {
                    ele.style.display = "block";
                    text.innerHTML = "Hide Advanced Options";
                }
            } 
        </script>

    </head>

    <body>

        <div id="wrapper">

            <?php include('includes/header.php'); ?>

            <?php include('includes/nav.php'); ?>

            <div id="content">
                <h3>Find Friend</h3><br>
                    <form name="form2" method="post" action="find_friend_results.php">
                        <h4>By RUID:</h4>
                        <table><tr><td>RUID:</td><td><input name="RUID" type="text" id="RUID"></td><td> <input type="submit" name="Submit" value="Search"></td></tr></table>

                    </form>
                    <br></br>
                    <a id="displayText" href="javascript:toggle();">Don't know their RUID? Try using Advanced Options</a>
                    <br></br>
                    <div id="toggleText" style="display: none">
                        <form name="form1" method="post" action="find_friend_results.php">
                            <?php
                            echo "<h4>By Profile Info:</h4>";
                            generateSearchFields("Users");
                            echo "<br>";
                            echo "<h4>By Relationships:</h4>";
                            generateSearchFields("Relationships");
                            echo "<br>";
                            echo "<h4>By Employment:</h4>";
                            generateSearchFields("Employment_Experience");
                            echo "<br>";
                            echo "<h4>By Housing:</h4>";
                            generateSearchFields("Housing");
                            echo "<br>";
                            echo "<h4>By Interests:</h4>";
                            generateSearchFields("Interests");
                            echo "<br>";
                            echo "<h4>By Courses:</h4>";
                            generateSearchFields("Enrolled");
                            echo "<br>";
                            echo "<h4>By Clubs:</h4>";
                            generateSearchFields("ClubMembers");
                            echo "<br>";
                            echo "<h4>By Study Groups:</h4>";
                            generateSearchFields("StudyGroupMembers");
                            echo "<br>";
                            echo "<h4>By Parties:</h4>";
                            generateSearchFields("PartiesMembers");
                            ?>
                            <br></br>
                            <table>
                                <tr><td>AND Relationship: <input type="hidden" name="ao" value="OR" /><input type="checkbox" name="ao" value="AND" /></td></tr>
                                <tr><td><input type="submit" name="Submit" value="Search"/></td></tr>
                            </table>
                        </form>
                    </div>
            </div> <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>

            <?php include('includes/footer.php'); ?>

        </div> <!-- End #wrapper -->

    </body>

</html>