<?php
include('includes/before_html.php');
$db = mysql_connect(db_server, $db_user, $db_password);
if (!$db) {
    //die('Could Not Connect: ' . mysql_error());
} else {
    //echo "Connected Successfully...\n";
}

mysql_select_db($db_dbname);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />

        <meta name="keywords" content="" />

        <meta name="author" content="" />

        <?php include('includes/head.php'); ?>

        <title><?php echo $heading ?></title>

    </head>

    <body>

        <div id="wrapper">

            <?php include('includes/header.php'); ?>

            <?php include('includes/nav.php'); ?>

            <div id="content">
                <?php
                if (!empty($_POST['RUID'])) {
                    $sql = "SELECT Users.FirstName, Users.LastName, Users.RUID, F.Status FROM Users LEFT JOIN Friends F ON F.Person1=$myRuid AND F.Person2=Users.RUID WHERE Users.RUID=" . $_POST['RUID'];
                    $changed = 1;
                } else {
                    $ao = 'AND';
                    if (isset($_POST['ao']))
                        $ao = $_POST['ao'];
                    $sql = '';
                    $p = 0;
                    $init = 1;
                    $temp = '';
                    $prefix = '';
                    $changed = 0;
                    $first = 1;
                    $tables = array('Users', 'Relationships', 'Employment_Experience', 'Housing', 'Interests', 'Enrolled', 'ClubMembers', 'StudyGroupMembers','PartiesMembers');
                    $excluded = array('Person1', 'Person2', 'Since', 'id');
                    foreach ($tables as $table) {
                        $temp = '';
                        $colnames = mysql_query("describe $table");

                        while ($row = mysql_fetch_array($colnames)) {
                            if ($row['Field'] == 'Password' || ($table != 'Users' && $row['Field'] == 'RUID') || in_array($row['Field'], $excluded)) {
                                // Do nothing
                            } else {
                                if (!empty($_POST[$table . "_" . $row['Field']])) {
                                    if ($init) {
                                        $sql = 'SELECT Users.FirstName, Users.LastName, Users.RUID, F.Status FROM Users LEFT JOIN Friends F ON F.Person1=' . $myRuid . ' AND F.Person2=Users.RUID WHERE (';
                                        $init = 0;
                                    }

                                    $changed = 1;
                                    if ($temp == '') {
                                        $temp .= $table . "." . $row['Field'] . " LIKE '%" . $_POST[$table . "_" . $row['Field']] . "%' ";
                                    }
                                    else
                                        $temp .= " $ao " . $table . "." . $row['Field'] . " LIKE '%" . $_POST[$table . "_" . $row['Field']] . "%' ";
                                }
                                //echo "<td><input name='" . $table . "_" . $row['Field'] . "' type='text' id='" . $table . "_" . $row['Field'] . "'></td>\n";
                            }
                        }
                        //echo "<br>temp=$temp<br>";
                        if (!empty($temp)) {
                            $sql.=$prefix;

                            if ($table == 'Relationships')
                                $sql.="SELECT $table.Person1 FROM " . $table . " WHERE ";
                            else if ($table != 'Users')
                                $sql.="SELECT $table.RUID FROM " . $table . " WHERE ";

                            $sql.=$temp;

                            if (!empty($prefix))
                                $p++;

                            if ($first) {
                                if ($changed)
                                    $prefix = "$ao Users.RUID IN ( ";
                                else
                                    $prefix = "Users.RUID IN ( ";
                                $first = 0;
                            }
                            else if ($table == 'Relationships')
                                $prefix = "$ao $table.Person1 IN ( ";
                            else {
                                if ($changed)
                                    $prefix = "$ao $table.RUID IN ( ";
                                else
                                    $prefix = "$table.RUID IN ( ";
                            }
                        }
                        else if (empty($temp) && $table == 'Users') {
                            $prefix = "Users.RUID IN ( ";
                        }
                    }

                    for ($i = 0; $i < $p; $i++) {
                        $sql.=')';
                    }
                    
                    $sql.=')';
                }
                $sql.=' AND Users.RUID <> ' . $myRuid . ' ORDER BY Users.FirstName';

                if (!$changed) {
                    echo "No search terms were entered.<br></br>";
                    echo "<a href='find_friend.php'>Return to search</a>";
                } else {

                    echo "<strong>Generated SQL Query:</strong> " . $sql . "<BR><BR>";
                    $db = mysql_connect(db_server, $db_user, $db_password);
                    mysql_select_db($db_dbname);

                    $result = mysql_query($sql);

                    //output data in a table
                    $output = '';
                    $output.= "<div id='output'>\n";
                    $output.= "<table width='100%'>\n";
                    $output.= "<tr>";
                    for ($i = 0; $i < mysql_num_fields($result); $i++) {
                        $field_info = mysql_fetch_field($result, $i);
                        $output.= "<th>{$field_info->name}</th>";
                    }
                    $output.="<th colspan='2'>Action</th>";
                    $output.= "</tr>";
                    while ($row = mysql_fetch_array($result)) {
                        $output.= "<tr>\n";
                        for ($i = 0; $i < count($row) / 2; $i++) {
                            $output.= "<td>$row[$i]</td>\n";
                        }
                        $output.="<td><a href='dashboard.php?ruid=" . $row['RUID'] . "'>View Profile</a></td>";
                        if ($row['Status'] == 'Accepted')
                            $output.="<td><a href='friend_request_action.php?rid=" . $row['RUID'] . "&a=0&f=1'>Unfriend</a></td>";
                        else
                            $output.="<td><a href='request_friend.php?rid=" . $row['RUID'] . "'>Request Friend</a></td>";

                        $output.= "</tr>\n";
                    }
                    $output.= '</table></div>';

                    $count = mysql_num_rows($result);

                    if ($count > 0)
                        echo $output;
                    else
                        echo "No results returned.";
                }
                ?>

            </div> <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>

            <?php include('includes/footer.php'); ?>

        </div> <!-- End #wrapper -->

    </body>

</html>