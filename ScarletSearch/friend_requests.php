<?php include('includes/before_html.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />

        <meta name="keywords" content="" />

        <meta name="author" content="" />

        <?php include('includes/head.php'); ?>

        <title><?php echo $heading ?></title>

    </head>

    <body>

        <div id="wrapper">

            <?php include('includes/header.php'); ?>

            <?php include('includes/nav.php'); ?>

            <div id="content">
                <?php
                echo "<h3>Friend Requests</h3>";
                $db = mysql_connect(db_server, $db_user, $db_password);
                mysql_select_db($db_dbname);

                $result = mysql_query("SELECT U.FirstName, U.LastName, F.Person1 as RUID, F.Status FROM Friends F, Users U WHERE Person1=U.RUID AND F.Person2 =" . $_COOKIE['ruid'] . " AND F.Status='Request'");

                $output = '';
                $output.= "<div id='output'>\n";
                $output.= "<table width='100%'>\n";
                $output.= "<tr>";
                for ($i = 0; $i < mysql_num_fields($result); $i++) {
                    $field_info = mysql_fetch_field($result, $i);
                    $output.= "<th>{$field_info->name}</th>";
                }
                $output.="<th colspan='2'>Action</th>";
                $output.= "</tr>";
                while ($row = mysql_fetch_array($result)) {
                    $output.= "<tr>\n";
                    for ($i = 0; $i < count($row) / 2; $i++) {
                        $output.= "<td>$row[$i]</td>\n";
                    }
                    
                    if($row['Status']=="Request")
                        $temp = 'Ignore';
                    else
                        $temp = 'Remove';
                    
                    $output.="<td><a href='friend_request_action.php?rid=" . $row['RUID'] . "&a=1'>Accept</a></td>";
                    $output.="<td><a href='friend_request_action.php?rid=" . $row['RUID'] . "&a=0'>$temp</a></td>";
                    $output.= "</tr>\n";
                }
                $output.= '</table></div>';

                $count = mysql_num_rows($result);

                if ($count > 0)
                    echo $output;
                else
                    echo "No friend requests.";
                
                echo "<br></br>";
                echo "<h3>Sent Requests</h3>";
                outputQueryResults("SELECT U.FirstName, U.LastName, U.RUID, F.Status FROM Friends F, Users U WHERE U.RUID=F.Person2 AND F.Person1=$myRuid AND F.Status='Request'");
                ?>
            </div> <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>

            <?php include('includes/footer.php'); ?>

        </div> <!-- End #wrapper -->

    </body>

</html>