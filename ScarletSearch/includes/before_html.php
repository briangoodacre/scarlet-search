<?php

include('variables/variables.php');
if (!isset($_COOKIE["user"]))
    header('Location: main_login.php');
else {
    setcookie("user", $_COOKIE["user"], time() + 3600);
    setcookie("ruid", $_COOKIE["ruid"], time() + 3600);
    $myRuid = $_COOKIE["ruid"];
    $myName = $_COOKIE["user"];
}

$friend_page = 0;   // set to 1 if trying to view a friend's page and not your own page

include('includes/dbInfo.php');

if (isset($_GET["ruid"])) {
    $getRuid = $_GET["ruid"];
    //if the ruid of the person you want to view is not yours, check if you are friends
    if ($_GET["ruid"] != $_COOKIE["ruid"]) {
        $friend_page = 1;
        $db = mysql_connect(db_server, $db_user, $db_password);
        mysql_select_db($db_dbname);

        $result1 = mysql_query("SELECT U.FirstName, U.LastName FROM Users U WHERE U.RUID=$getRuid");
        $row = mysql_fetch_array($result1);
        $getName = $row['FirstName'] . " " . $row['LastName'];

        $result = mysql_query("SELECT * FROM Friends WHERE Person1=" . $_COOKIE["ruid"] . " AND Person2=" . $_GET["ruid"] . " AND Status='Accepted'");
        $count = mysql_num_rows($result);
        if ($count <= 0)
            header('Location: notfriends.php?rid=' . $_GET["ruid"]);
    }
    else {
        $getRuid = $myRuid;
        $getName = $myName;
    }
} else {
    $getRuid = $myRuid;
    $getName = $myName;
}

function getName($ruid) {
    $db = mysql_connect(db_server, $db_user, $db_password);
    mysql_select_db($db_dbname);

    $result1 = mysql_query("SELECT U.FirstName, U.LastName FROM Users U WHERE U.RUID=$ruid");
    $row = mysql_fetch_array($result1);
    $name = $row['FirstName'] . " " . $row['LastName'];

    return $name;
}

function checkSecurity($field) {
    $db = mysql_connect(db_server, $db_user, $db_password);
    mysql_select_db($db_dbname);
    $result = mysql_query("select * from Security where RUID=" . $_GET["ruid"]);
    $count = mysql_num_rows($result);
    if ($count > 0) {
        $row = mysql_fetch_array($result);
        if ($row[$field] == 0)
            header('Location: private.php?ruid=' . $_GET["ruid"]);
    } else {
        // Do nothing
    }
}

/**
 * Run MySQL query and output 
 * results in a HTML Table
 */
function outputQueryResults($select_query) {

    //run a select query
    //$select_query = 'SELECT FirstName, LastName, RUID FROM Users';
    $result = mysql_query($select_query);

    //output data in a table
    $output = '';
    $output.= "<div id='output'>\n";
    $output.= "<table width='100%'>\n";
    $output.= "<tr>";
    for ($i = 0; $i < mysql_num_fields($result); $i++) {
        $field_info = mysql_fetch_field($result, $i);
        $output.= "<th>{$field_info->name}</th>";
    }
    $output.= "</tr>";
    while ($row = mysql_fetch_row($result)) {
        $output.= "<tr>\n";
        foreach ($row as $val) {
            $output.= "<td>$val</td>\n";
        }
        $output.= "</tr>\n";
    }
    $output.= '</table></div>';

    $count = mysql_num_rows($result);
    if ($count <= 0)
        return 0;
    else {
        echo $output;
        return 1;
    }
}

function generateSearchFields($table) {
    $excluded = array('Person1', 'Person2', 'Since', 'id', 'RUID', 'MemberRUID');
    $colnames = mysql_query("describe $table");
    //<input name="myusername" type="text" id="myusername">
    echo "<table>";
    while ($row = mysql_fetch_array($colnames)) {
        if ($row['Field'] == 'Password' || in_array($row['Field'], $excluded)) {
            // Do nothing
        } else {
            echo "<tr>\n";
            echo "<td>" . $row['Field'] . ": </td>\n";
            echo "<td><input name='" . $table . "_" . $row['Field'] . "' type='text' id='" . $table . "_" . $row['Field'] . "'></td>\n";
            echo "</tr>\n";
        }
    }
    echo "</table>";
}

?>
