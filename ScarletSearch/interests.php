<?php
$table = 'Interests';
include('includes/before_html.php');
if ($friend_page)
    checkSecurity($table);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <title><?php echo $heading ?></title>
        <?php include('includes/head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('includes/header.php'); ?>
            <?php include('includes/nav.php'); ?>
            <div id="content">

                <?php
                echo "<h3>" . $getName . "'s $table</h3><BR>";
                $db = mysql_connect(db_server, $db_user, $db_password);
                mysql_select_db($db_dbname);
                $sql = "SELECT * FROM $table WHERE RUID = " . $getRuid;
                echo "SQL QUERY:&nbsp;" . $sql . "<BR><BR>";
                $result2 = mysql_query($sql);
                $count = mysql_num_rows($result2);

                if ($count == 0) {
                    echo "No interests.";
                }

                while ($row = mysql_fetch_array($result2)) {
                    echo "<table bgcolor='#FFFFFF'>";
                    $result = mysql_query("describe $table");
                    while ($colNames = mysql_fetch_array($result)) {
                        if ($colNames['Field'] != 'RUID') {
                            echo "<tr><td><strong>{$colNames['Field']}</strong>:  " . $row[$colNames['Field']] . "</td></tr>";
                            if ($getRuid == $myRuid) {
                                echo "<tr><td>";
                                $sql = "SELECT U.FirstName, U.LastName, U.RUID FROM Users U, Interests I, Friends F WHERE I.RUID=U.RUID AND I.RUID=F.Person2 AND F.Person1=$getRuid AND F.Status='Accepted' AND I.InterestName='{$row[$colNames['Field']]}'";
                                $r = mysql_query($sql);

                                echo "Friends with same interest:  ";
                                while ($r1 = mysql_fetch_array($r)) {
                                    echo "<a href='dashboard.php?ruid={$r1['RUID']}'>" . $r1['FirstName'] . " " . $r1['LastName'] . ";</a>  ";
                                }
                                echo "</td></tr>";
                                echo "<tr><td>SQL Query: $sql</td></tr>";
                            }
                        }
                    }
                    echo "</table><BR><BR>";
                }

                mysql_close($db)
                ?>
            </div>
            <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>
            <?php include('includes/footer.php'); ?>
        </div>
        <!-- End #wrapper -->

    </body>
</html>