<?php

session_start();
session_destroy();
setcookie("user", "", time()-3600);
setcookie("ruid", "", time()-3600);
?>
<?php include('includes/before_login.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />

        <meta name="keywords" content="" />

        <meta name="author" content="" />

        <link rel="stylesheet" type="text/css" href="style.css" media="screen" />

        <title><?php echo $heading ?></title>
        
        <meta http-equiv="refresh" content="0; URL=index.php">

    </head>

    <body>

        <div id="wrapper">

            <?php include('includes/header.php'); ?>

            <?php include('includes/nav.php'); ?>

            <div id="content">
               You have logged out!  Redirecting...
            </div> <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>

            <?php include('includes/footer.php'); ?>

        </div> <!-- End #wrapper -->

    </body>

</html>