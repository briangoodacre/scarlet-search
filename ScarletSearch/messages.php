<?php include('includes/before_html.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />

        <meta name="keywords" content="" />

        <meta name="author" content="" />

        <?php include('includes/head.php'); ?>

        <title><?php echo $heading ?></title>

    </head>

    <body>

        <div id="wrapper">

            <?php include('includes/header.php'); ?>

            <?php include('includes/nav.php'); ?>

            <div id="content">
                <h3>Messages</h3><br>
                    <a href="compose_message.php">Compose Message</a> <br></br>
                    <?php
                    $db = mysql_connect(db_server, $db_user, $db_password);
                    mysql_select_db($db_dbname);
                    
                    $result = mysql_query("SELECT M.id, U.FirstName, U.LastName, M.Subject, M.Date from Messages M, Users U WHERE U.RUID=M.FromID AND M.ToID={$myRuid} AND M.Owner={$myRuid}");
                    $count = mysql_num_rows($result);
                    if ($count <= 0)
                        echo "You have no messages.";
                    else {
                        echo "<div id='output'><table width='98%'><tr><th>Actions</th><th>From</th><th>Subject</th><th>Date</th></tr>";
                        
                        while($row = mysql_fetch_array($result)){       
                            echo "<tr>";
                            echo "<td><a href='compose_message.php?replyto=".$row['id']."'>Reply</a> | <a href='delete_message.php?id=".$row['id']."'>Delete</a></td>";
                            echo "<td><a href='msg.php?id=".$row['id']."'>".$row['FirstName']." ".$row['LastName']."</a></td>";
                            echo "<td><a href='msg.php?id=".$row['id']."'>".$row['Subject']."</a></td>";
                            echo "<td><a href='msg.php?id=".$row['id']."'>".$row['Date']."</a></td>";
                            echo "</tr>";
                        }
                        echo "</table></div>";
                        
                    }
                    echo "<br></br>";
                    echo "<h3>Sent Messages</h3>";
                    $result = mysql_query("SELECT M.id, U.FirstName, U.LastName, M.Subject, M.Date from Messages M, Users U WHERE U.RUID=M.ToID AND M.FromID={$myRuid} AND M.Owner={$myRuid}");
                    $count = mysql_num_rows($result);
                    if ($count <= 0)
                        echo "You have no messages.";
                    else {
                        echo "<div id='output'><table width='98%'><tr><th>Actions</th><th>Sent To</th><th>Subject</th><th>Date</th></tr>";
                        
                        while($row = mysql_fetch_array($result)){       
                            echo "<tr>";
                            echo "<td><a href='compose_message.php?replyto=".$row['id']."'>Reply</a> | <a href='delete_message.php?id=".$row['id']."'>Delete</a></td>";
                            echo "<td><a href='msg.php?id=".$row['id']."'>".$row['FirstName']." ".$row['LastName']."</a></td>";
                            echo "<td><a href='msg.php?id=".$row['id']."'>".$row['Subject']."</a></td>";
                            echo "<td><a href='msg.php?id=".$row['id']."'>".$row['Date']."</a></td>";
                            echo "</tr>";
                        }
                        echo "</table></div>";
                        
                    }
                    ?>
                    
                   
            </div> <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>

            <?php include('includes/footer.php'); ?>

        </div> <!-- End #wrapper -->

    </body>

</html>