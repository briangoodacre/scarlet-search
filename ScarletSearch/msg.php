<?php
include('includes/before_html.php');

$db = mysql_connect(db_server, $db_user, $db_password);
if (!$db) {
    //die('Could Not Connect: ' . mysql_error());
} else {
    //echo "Connected Successfully...\n";
}

mysql_select_db($db_dbname);

$result = mysql_query("SELECT ToID, FromID FROM Messages WHERE id=" . $_GET['id']);
$row = mysql_fetch_array($result);

if ($row['ToID'] != $myRuid && $row['FromID'] != $myRuid)
    header('Location: invalid.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />

        <meta name="keywords" content="" />

        <meta name="author" content="" />

        <?php include('includes/head.php'); ?>

        <title><?php echo $heading ?></title>

    </head>

    <body>

        <div id="wrapper">

            <?php include('includes/header.php'); ?>

            <?php include('includes/nav.php'); ?>

            <div id="content">
                <?php
                $result = mysql_query("SELECT * FROM Messages WHERE id = " . $_GET['id']);

                while ($row = mysql_fetch_array($result)) {
                    echo "<table>";
                    echo "<tr><td>To:</td><td>" . getName($row['ToID']) . "</td></tr>";
                    echo "<tr><td>From:</td><td>" . getName($row['FromID']) . "</td></tr>";
                    echo "<tr><td>Subject:</td><td>" . $row['Subject'] . "</td></tr>";
                    if ($row['ReplyTo'] != '')
                        echo "<tr><td>RE:</td><td><a href='msg.php?id=" . $row['ReplyTo'] . "'>Original Message</a></td></tr>";
                    echo "<tr><td>Body:</td><td>" . $row['Body'] . "</td></tr>";
                    echo "<tr><td><a href='compose_message.php?replyto=".$row['id']."'>Reply</a></td><td></td></tr>";

                    echo "</table><BR>";
                }
                mysql_close($db)
                ?>
            </div> <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>

            <?php include('includes/footer.php'); ?>

        </div> <!-- End #wrapper -->

    </body>

</html>