<?php 
$table='Parties';
include('includes/before_html.php'); 
if ($friend_page)
    checkSecurity($table);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <?php include('includes/head.php'); ?>
        <title><?php echo $heading ?></title>

    </head>

    <body>
        <div id="wrapper">
            <?php include('includes/header.php'); ?>
            <?php include('includes/nav.php'); ?>
            <div id="content">
		<?php
		 $db = mysql_connect(db_server, $db_user, $db_password);
                mysql_select_db($db_dbname);
		
		
		
		#Parties Hosting
                echo "<h3>".$getName."'s Hosting Parties</h3><BR>";
                $sql = "SELECT DISTINCT P.PartyName,P.Time,P.Description,P.BuildingName,P.RoomNumber,U.FirstName,U.LastName
		        FROM Parties P, Users U
			WHERE U.RUID=P.HostRUID AND 
			      P.HostRUID = " . $getRuid;
                echo "SQL QUERY:&nbsp;" . $sql . "<BR><BR>";
                $result2 = mysql_query($sql);
                $count = mysql_num_rows($result2);

                if ($count == 0) {
                    echo "No Hosted Parties.";
                }
                echo "<table>";
		#Just temporary of what is inside the while loop
		$sql_attend = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
			           FROM Parties P, PartiesMembers M
			           WHERE P.PartyName=M.PartyName
				         AND P.PartyName = '".$row['PartyName']."' 
			           GROUP BY P.PartyName";
		$sql_friends = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
		                    FROM Parties P, PartiesMembers M
		        		WHERE P.PartyName=M.PartyName
					      AND P.PartyName = '".$row['PartyName']."' 
		        		      AND M.RUID IN (SELECT DISTINCT U.RUID 
								   FROM Friends F, Users U
		    	                                           WHERE F.Person1=".$getRuid." AND 
		    	                                           F.Person2=U.RUID AND 
			                                           F.Status='Accepted')
				   GROUP BY P.PartyName";
		echo "SQL QUERY(people attending):&nbsp;" . $sql_attend . "<BR><BR>";
		echo "SQL QUERY(friends attending):&nbsp;" . $sql_friends . "<BR><BR>";
		$count_while=0;
                while ($row = mysql_fetch_array($result2)) {
		    $sql_friends = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
		                    FROM Parties P, PartiesMembers M
		        		WHERE P.PartyName=M.PartyName
					      AND P.PartyName = '".$row['PartyName']."' 
		        		      AND M.RUID IN (SELECT DISTINCT U.RUID 
								   FROM Friends F, Users U
		    	                                           WHERE F.Person1=".$getRuid." AND 
		    	                                           F.Person2=U.RUID AND 
			                                           F.Status='Accepted')
				   GROUP BY P.PartyName";
		$result_friends = mysql_query($sql_friends);
		$row_friends = mysql_fetch_array($result_friends);
		    #People Attending a Party
		    $sql_attend = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
			           FROM Parties P, PartiesMembers M
			           WHERE P.PartyName=M.PartyName
				         AND P.PartyName = '".$row['PartyName']."' 
			           GROUP BY P.PartyName";
		$result_attend = mysql_query($sql_attend);
		$row_attend = mysql_fetch_array($result_attend);
		    echo "<table>"; {
		    #Friends Attending a Party
                        echo "<tr><td><strong> Party: " . $row['PartyName'] . " </strong></td></tr>";
			if($row_attend['Num']==null)
			    echo "<tr><td> People Attending: 0</td></tr>";
			else
			    echo "<tr><td> People Attending: " . $row_attend['Num'] . " </td></tr>";
			if($row_friends['Num']==null)
			    echo "<tr><td> Friends Attending: 0</td></tr>";
			else
			    echo "<tr><td> Friends Attending: " . $row_friends['Num'] . " </td></tr>";
                        echo "<tr><td> Host: " . $row['FirstName'] . " " . $row['LastName'] . " </strong></td></tr>";
                        echo "<tr><td> Time: " . $row['Time'] . " </td></tr>";
			echo "<tr><td> Description: " . $row['Description'] . " </td></tr>";
                        echo "<tr><td> Building: " . $row['BuildingName'] . " </td></tr>";
			echo "<tr><td> RoomNumber: " . $row['RoomNumber'] . " </td></tr>";
		    }
		    echo "</table><BR>";
                }
                echo "</table><BR>";
		
		#Parties Attending
		echo "<h3>".$getName."'s Attending Parties</h3><BR>";
                $sql = "SELECT DISTINCT P.PartyName, P.Description, P.Time, P.BuildingName, P.RoomNumber 
		        FROM Parties P, PartiesMembers M 
			WHERE M.PartyName = P.PartyName AND M.RUID = " . $getRuid ;
                echo "SQL QUERY:&nbsp;" . $sql . "<BR><BR>";
		#Just temporary of what is inside the while loop
		$sql_attend = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
			           FROM Parties P, PartiesMembers M
			           WHERE P.PartyName=M.PartyName
				         AND P.PartyName = '".$row['PartyName']."' 
			           GROUP BY P.PartyName";
		$sql_friends = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
		                    FROM Parties P, PartiesMembers M
		        		WHERE P.PartyName=M.PartyName
					      AND P.PartyName = '".$row['PartyName']."' 
		        		      AND M.RUID IN (SELECT DISTINCT U.RUID 
								   FROM Friends F, Users U
		    	                                           WHERE F.Person1=".$getRuid." AND 
		    	                                           F.Person2=U.RUID AND 
			                                           F.Status='Accepted')
				   GROUP BY P.PartyName";
		echo "SQL QUERY(people attending):&nbsp;" . $sql_attend . "<BR><BR>";
		echo "SQL QUERY(friends attending):&nbsp;" . $sql_friends . "<BR><BR>";
                $result2 = mysql_query($sql);
                $count = mysql_num_rows($result2);

                if ($count == 0) {
                    echo "No Attending Parties.";
                }
                echo "<table>";
                while ($row = mysql_fetch_array($result2)) {
		    $sql_friends = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
		                    FROM Parties P, PartiesMembers M
		        		WHERE P.PartyName=M.PartyName
					      AND P.PartyName = '".$row['PartyName']."' 
		        		      AND M.RUID IN (SELECT DISTINCT U.RUID 
								   FROM Friends F, Users U
		    	                                           WHERE F.Person1=".$getRuid." AND 
		    	                                           F.Person2=U.RUID AND 
			                                           F.Status='Accepted')
				   GROUP BY P.PartyName";
		    $result_friends = mysql_query($sql_friends);
		    $row_friends = mysql_fetch_array($result_friends);
		    #People Attending a Party
		    $sql_attend = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
			           FROM Parties P, PartiesMembers M
			           WHERE P.PartyName=M.PartyName
				         AND P.PartyName = '".$row['PartyName']."' 
			           GROUP BY P.PartyName";
		    $result_attend = mysql_query($sql_attend);
		    $row_attend = mysql_fetch_array($result_attend);
                    echo "<table>"; {
                        echo "<tr><td><strong> Party: " . $row['PartyName'] . " </strong></td></tr>";
			if($row_attend['Num']==null)
			    echo "<tr><td> People Attending: 0</td></tr>";
			else
			    echo "<tr><td> People Attending: " . $row_attend['Num'] . " </td></tr>";
			if($row_friends['Num']==null)
			    echo "<tr><td> Friends Attending: 0</td></tr>";
			else
			    echo "<tr><td> Friends Attending: " . $row_friends['Num'] . " </td></tr>";
                        echo "<tr><td> Host: " . $row['FirstName'] . " " . $row['LastName'] . " </strong></td></tr>";
                        echo "<tr><td> Time: " . $row['Time'] . " </td></tr>";
			echo "<tr><td> Description: " . $row['Description'] . " </td></tr>";
                        echo "<tr><td> Building: " . $row['Building'] . " </td></tr>";
			echo "<tr><td> RoomNumber: " . $row['RoomNumber'] . " </td></tr>";
                    }
                    echo "</table><BR>";
                }
                echo "</table><BR>";
		
		#Search for a Party to Attend
		echo "<h3>".$getName."'s Available Parties to Attend</h3><BR>";
		#list of parties friends are attending
		$sql = "SELECT DISTINCT P.PartyName, P.Description, P.Time, P.BuildingName, P.RoomNumber, U2.FirstName, U2.LastName
			FROM PartiesMembers PM, Parties P, Users U2
			WHERE P.HostRUID = U2.RUID AND 
			      PM.PartyName = P.PartyName AND 
			      PM.RUID IN (SELECT DISTINCT U.RUID 
						 FROM Friends F, Users U 
						 WHERE F.Person1= " . $_COOKIE['ruid'] . " AND 
						       F.Person2=U.RUID AND 
						       F.Status='Accepted') AND
                              PM.PartyName NOT IN (SELECT DISTINCT P1.PartyName 
			                           FROM Parties P1, PartiesMembers M1 
						   WHERE M1.PartyName = P1.PartyName AND 
						            M1.RUID = " . $getRuid ." )";
						 
		echo "SQL QUERY:&nbsp;" . $sql . "<BR><BR>";
		#Just temporary of what is inside the while loop
		$sql_attend = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
			           FROM Parties P, PartiesMembers M
			           WHERE P.PartyName=M.PartyName
				         AND P.PartyName = '".$row['PartyName']."' 
			           GROUP BY P.PartyName";
		$sql_friends = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
		                    FROM Parties P, PartiesMembers M
		        		WHERE P.PartyName=M.PartyName
					      AND P.PartyName = '".$row['PartyName']."' 
		        		      AND M.RUID IN (SELECT DISTINCT U.RUID 
								   FROM Friends F, Users U
		    	                                           WHERE F.Person1=".$getRuid." AND 
		    	                                           F.Person2=U.RUID AND 
			                                           F.Status='Accepted')
				   GROUP BY P.PartyName";
		echo "SQL QUERY(people attending):&nbsp;" . $sql_attend . "<BR><BR>";
		echo "SQL QUERY(friends attending):&nbsp;" . $sql_friends . "<BR><BR>";
		$result2 = mysql_query($sql);
                $count = mysql_num_rows($result2);

                if ($count == 0) {
                    echo "No Available Parties to Attend.";
                }
                echo "<table>";
                while ($row = mysql_fetch_array($result2)) {
		    $sql_friends = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
		                    FROM Parties P, PartiesMembers M
		        		WHERE P.PartyName=M.PartyName
					      AND P.PartyName = '".$row['PartyName']."' 
		        		      AND M.RUID IN (SELECT DISTINCT U.RUID 
								   FROM Friends F, Users U
		    	                                           WHERE F.Person1=".$getRuid." AND 
		    	                                           F.Person2=U.RUID AND 
			                                           F.Status='Accepted')
				   GROUP BY P.PartyName";
		    $result_friends = mysql_query($sql_friends);
		    $row_friends = mysql_fetch_array($result_friends);
		    #People Attending a Party
		    $sql_attend = "SELECT P.PartyName, COUNT(P.PartyName) AS Num
			           FROM Parties P, PartiesMembers M
			           WHERE P.PartyName=M.PartyName
				         AND P.PartyName = '".$row['PartyName']."' 
			           GROUP BY P.PartyName";
		    $result_attend = mysql_query($sql_attend);
		    $row_attend = mysql_fetch_array($result_attend);
                    echo "<table>"; {
                        echo "<tr><td><strong> Party: " . $row['PartyName'] . " </strong></td></tr>";
			if($row_attend['Num']==null)
			    echo "<tr><td> People Attending: 0</td></tr>";
			else
			    echo "<tr><td> People Attending: " . $row_attend['Num'] . " </td></tr>";
			if($row_friends['Num']==null)
			    echo "<tr><td> Friends Attending: 0</td></tr>";
			else
			    echo "<tr><td> Friends Attending: " . $row_friends['Num'] . " </td></tr>";
                        echo "<tr><td> Host: " . $row['FirstName'] . " " . $row['LastName'] . " </strong></td></tr>";
                        echo "<tr><td> Time: " . $row['Time'] . " </td></tr>";
			echo "<tr><td> Description: " . $row['Description'] . " </td></tr>";
                        echo "<tr><td> Building: " . $row['Building'] . " </td></tr>";
			echo "<tr><td> RoomNumber: " . $row['RoomNumber'] . " </td></tr>";
                    }
                    echo "</table><BR>";
                }
                echo "</table><BR>";
		
		 
                mysql_close($db)
                ?>
		
		<!--Join a Parties-->
		<h3>Join a Party</h3>
		<form action="parties.php" method="post">
		    *Party Name: <input type="text" name="j_name" /> <br>
	            <input type="submit" />
		    </form>
		
		<?php
		if (!empty($_POST['j_name'])) {
		    $db = mysql_connect(db_server, $db_user, $db_password);
		    mysql_select_db($db_dbname);
		    $sql = "INSERT INTO PartiesMembers VALUES ('".$_POST['j_name']."',
			                                           ".$getRuid.")";
		    echo "Joined Party!";
		    
		    echo $sql;
		    mysql_query($sql);
		    
		    mysql_close($db);
		  }?>
		
		<!--Create a Party-->
		<h3>Create a Party</h3>
		<form action="parties.php" method="post">
		    *Party Name: <input type="text" name="PartyName" /> <br>
		    Time(mm-dd-yy hh:mm:ss): <input type="text" name="Time" /><br>
		    Description: <input type="text" name="Description" /><br>
		    Building Name: <input type="text" name="Building Name" /><br>
		    Room Number: <input type="text" name="Room Number" /><br>
	            <input type="submit" />
		    </form>
		
		<?php
		if (!empty($_POST['PartyName'])) {
		    $db = mysql_connect(db_server, $db_user, $db_password);
		    mysql_select_db($db_dbname);
		    $sql = "INSERT INTO Parties VALUES ('".$_POST['PartyName']." ',
							".$getRuid.",
							'".$_POST['Time']."',
							'".$_POST['Description']." ',
							'".$_POST['BuildingName']." ',
							'".$_POST['RoomNumber']." ')";
		    
		    
		    echo $sql;
		    $result = mysql_query($sql);
                    if($result){
                        echo "Party Created!<br>";
                    }
                    else{
                        echo "Error creating party. ".mysql_error()."<br>";
                    }
		    $sql = "INSERT INTO PartiesMembers VALUES ('".$_POST['PartyName']."',".$getRuid.");";
		    echo $sql;
		    mysql_query($sql);
		    
		    mysql_close($db);
		  }?>
		
		
		
               
            </div> <!-- end #content -->
            <?php include('includes/sidebar.php'); ?>
            <?php include('includes/footer.php'); ?>
        </div> <!-- End #wrapper -->
    </body>
</html>