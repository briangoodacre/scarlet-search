<?php
include('includes/before_html.php');
if ($friend_page)
    checkSecurity('Profile');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <title><?php echo $heading ?></title>
        <?php include('includes/head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('includes/header.php'); ?>
            <?php include('includes/nav.php'); ?>
            <div id="content">
                <?php echo "<h3>".$getName."'s Profile</h3><BR>"; ?>
                <table>
                    <?php
                    $db = mysql_connect(db_server, $db_user, $db_password);
                   
                    mysql_select_db($db_dbname);
                    $result = mysql_query("SELECT * FROM Users WHERE RUID = " . $getRuid);
                    $row = mysql_fetch_array($result);
                    $result = mysql_query("describe Users");
                    while ($colNames = mysql_fetch_array($result)) {
                        if ($colNames['Field'] != "Password")
                            echo "<tr><td><strong>" . $colNames['Field'] . "</strong>:</td><td>" . $row[$colNames['Field']] . "</td></tr>";
                    }
                    mysql_close($db)
                    ?>
                </table>
            </div>
            <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>
            <?php include('includes/footer.php'); ?>
        </div>
        <!-- End #wrapper -->

    </body>
</html>