<?php
include('includes/before_html.php');

$db = mysql_connect(db_server, $db_user, $db_password);
if (!$db) {
    //die('Could Not Connect: ' . mysql_error());
} else {
    //echo "Connected Successfully...\n";
}

mysql_select_db($db_dbname);

$result = mysql_query("SELECT * FROM SavedQueries WHERE id=" . $_GET["id"]);
$row = mysql_fetch_array($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />

        <meta name="keywords" content="" />

        <meta name="author" content="" />

        <?php include('includes/head.php'); ?>

        <title><?php echo $row['PageTitle']; ?></title>

    </head>

    <body>

        <div id="wrapper">

            <?php include('includes/header.php'); ?>

            <?php include('includes/nav.php'); ?>

            <div id="content">

                <?php
                
                echo "<h3>".$row['PageTitle']."</h3><br>";
                if($row['Description'])
                    echo $row['Description']."<br><br>";
                outputQueryResults($row['Query']);

                mysql_close($db)
                ?>  



            </div> <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>

            <?php include('includes/footer.php'); ?>

        </div> <!-- End #wrapper -->

    </body>

</html>