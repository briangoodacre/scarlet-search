<?php
include('includes/before_html.php');
if ($friend_page)
    checkSecurity('Relationship');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <title><?php echo $heading ?></title>
        <?php include('includes/head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('includes/header.php'); ?>
            <?php include('includes/nav.php'); ?>
            <div id="content">
                <?php echo "<h3>" . $getName . "'s Relationships</h3><BR>"; ?>
                <?php
                $db = mysql_connect(db_server, $db_user, $db_password);
                mysql_select_db($db_dbname);
                $sql = "SELECT DISTINCT U.RUID, U.FirstName, U.LastName, R.RelationshipType, R.Since FROM Relationships R, Users U WHERE ((R.Person1 = " . $getRuid . " AND R.Person2=U.RUID) OR (R.Person2 = " . $getRuid . " AND R.Person1=U.RUID))";
                echo "SQL QUERY:&nbsp;" . $sql . "<BR><BR>";
                $result2 = mysql_query($sql);
                $count = mysql_num_rows($result2);

                if ($count == 0) {
                    echo "No relationships.";
                } else {


                    while ($row = mysql_fetch_array($result2)) {
                        echo "<table>";
                        echo "<tr><td><strong>Name</strong></td><td><a href='dashboard.php?ruid={$row['RUID']}'>{$row['FirstName']} {$row['LastName']}</a></td></tr>";
                        echo "<tr><td><strong>RUID</strong></td><td>{$row['RUID']}</td></tr>";
                        echo "<tr><td><strong>Relationship</strong></td><td>{$row['RelationshipType']}</td></tr>";
                        if (!empty($row['Since']))
                            echo "<tr><td><strong>Since</strong></td><td>{$row['Since']}</td></tr>";
                        echo "</table><BR><BR>";
                    }
                }


                mysql_close($db)
                ?>
            </div>
            <!-- end #content -->

            <?php include('includes/sidebar.php'); ?>
            <?php include('includes/footer.php'); ?>
        </div>
        <!-- End #wrapper -->

    </body>
</html>