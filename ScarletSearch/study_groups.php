<?php include('includes/before_html.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <?php include('includes/head.php'); ?>
        <title><?php echo $heading ?></title>

    </head>

    <body>
        <div id="wrapper">
            <?php include('includes/header.php'); ?>
            <?php include('includes/nav.php'); ?>
            <div id="content">
		<h3>Create a study group</h3>
		<form action="study_groups.php" method="post">
		    *Name: <input type="text" name="c_name" /> <br>
		    *Course ID: <input type="text" name="c_courseid" /><br>
		    MeetingTimes: <input type="text" name="c_meetingtimes" /><br>
		    Max_Size: <input type="text" name="c_maxsize" /><br>
		    Location: <input type="text" name="c_location" /><br>
		    
	            <input type="submit" />
		    </form>
		
		<?php
		if (!empty($_POST['c_name'])) {
		    $db = mysql_connect(db_server, $db_user, $db_password);
		    mysql_select_db($db_dbname);
		    if($_POST['c_maxsize']==NULL)
			$c_maxsize=10;
		    else
			$c_maxsize=$_POST['c_maxsize'];
		    $sql = "INSERT INTO StudyGroups VALUES ('".$_POST['c_name']."',
			                                    '".$_POST['c_meetingtimes']." ',
			                                     ".$getRuid.",
			                                     ".$c_maxsize.",
						            '".$_POST['c_location']." ',
							    ".$_POST['c_courseid'].")";
		    echo "Study Group Created!<br>";
		    
		    echo $sql;
		    mysql_query($sql);
		    $sql = "INSERT INTO StudyGroupMembers VALUES ('".$_POST['c_name']."',
			                                           ".$getRuid.")";
		    echo "Joined Study Group!<br>";
		    
		    echo $sql;
		    mysql_query($sql);
		    
		    
		    mysql_close($db);
		  }?>
		
		<h3>Join a study group</h3>
		<form action="study_groups.php" method="post">
		    *GroupName: <input type="text" name="j_name" /> <br>
	            <input type="submit" />
		    </form>
		
		<?php
		if (!empty($_POST['j_name'])) {
		    $db = mysql_connect(db_server, $db_user, $db_password);
		    mysql_select_db($db_dbname);
		    $sql = "INSERT INTO StudyGroupMembers VALUES ('".$_POST['j_name']."',
			                                           ".$getRuid.")";
		    echo "Joined Study Group!";
		    
		    echo $sql;
		    mysql_query($sql);
		    
		    mysql_close($db);
		  }?>
		
		<h3>Your current study groups</h3>
		<?php
                    $db = mysql_connect(db_server, $db_user, $db_password);
                    mysql_select_db($db_dbname);
                    $sql="SELECT DISTINCT S.Name, S.MeetingTimes,S.Leader,S.Max_Size,S.Location,S.CourseId 
			  FROM StudyGroups S, StudyGroupMembers M
			  WHERE S.Name=M.GroupName AND 
			        M.RUID = " . $getRuid;
                    echo "SQL QUERY:&nbsp;".$sql."<BR>";
                    $result2 = mysql_query($sql);
                    $count = mysql_num_rows($result2);
                    
                    if($count==0){
                        echo "No current study groups.";
                    }
                        
                    while ($row = mysql_fetch_array($result2)) {
                        echo "<table>";
                          {
                              echo "<tr><td><strong> Name: ". $row['Name']. " </strong></td></tr>";
			      echo "<tr><td> CourseId: ". $row['CourseId']."  </strong></td></tr>";
			      echo "<tr><td> Leader: ". $row['Leader']."  </strong></td></tr>";
			      echo "<tr><td> MeetingTimes: ". $row['MeetingTimes']. " </td></tr>";
			      echo "<tr><td> Location: ". $row['Location']. " </td></tr>";
			      echo "<tr><td> Max_Size: ". $row['Max_Size']. " </td></tr>";
                          }
                        echo "</table><BR>";
                    }
                    mysql_close($db)
                    ?>
		
		<h3>New study groups based on your courses</h3>
		<?php
                    $db = mysql_connect(db_server, $db_user, $db_password);
                    mysql_select_db($db_dbname);
                    $sql="SELECT DISTINCT S.Name, S.MeetingTimes,S.Leader,S.Max_Size,S.Location,S.CourseId 
			  FROM StudyGroups S, StudyGroupMembers M, Enrolled E
			  WHERE S.CourseId = E.CourseId AND
				S.Name NOT IN (SELECT DISTINCT M1.GroupName 
			                       FROM StudyGroupMembers M1
			                       WHERE M1.RUID = " . $getRuid.") AND 
			        E.RUID = " . $getRuid;
                    echo "SQL QUERY:&nbsp;".$sql."<BR>";
                    $result2 = mysql_query($sql);
                    $count = mysql_num_rows($result2);
                    
                    if($count==0){
                        echo "No suggested study groups.";
                    }
                        
                    while ($row = mysql_fetch_array($result2)) {
                        echo "<table>";
                          {
                              echo "<tr><td><strong> Name: ". $row['Name']. " </strong></td></tr>";
			      echo "<tr><td> CourseId: ". $row['CourseId']."  </strong></td></tr>";
			      echo "<tr><td> Leader: ". $row['Leader']."  </strong></td></tr>";
			      echo "<tr><td> MeetingTimes: ". $row['MeetingTimes']. " </td></tr>";
			      echo "<tr><td> Location: ". $row['Location']. " </td></tr>";
			      echo "<tr><td> Max_Size: ". $row['Max_Size']. " </td></tr>";
                          }
                        echo "</table><BR>";
                    }
                    mysql_close($db)
                    ?>
		
		
		
            </div> <!-- end #content -->
            <?php include('includes/sidebar.php'); ?>
            <?php include('includes/footer.php'); ?>
        </div> <!-- End #wrapper -->
    </body>
</html>